new Vue({
    el: '#db-drawer',
    data: function () {
        return {
            currentTable: '0',
            currentColumn: '0',
            startRelation: '',
            tables: [],
            columns: [
                {
                    name: 'name',
                    field: 'name',
                    align: 'left'
                },
            ],
            columnTypes: ['int', 'string', 'enum'],
            selectedTable: false,
            selectedColumn: false,
            selectedRelation: [],
        }
    },
    methods: {
        showSettings: function (index, id) {
            let _this = this;
            let columnIndex = _this.tables[_this.currentTable].data.findIndex(obj => obj.id == id);
            _this.currentColumn = columnIndex;
        },
        emptyColumn: function () {
            return {
                name: '',
                pk: false,
                ai: false,
                nn: false,
                type: 'string',
                id: this.generateID()
            }
        },
        addTable: function () {
            let _this = this;
            let newTable = {
                id: _this.generateID(),
                title: 'table ' + (parseInt(_this.tables.length) + 1),
                relations: [],
                x: 0,
                y: 0,
                data: [
                    _this.emptyColumn()
                ]
            };
            _this.tables.push(newTable);
        },
        generateID: function () {
            return Math.random().toString(36).substr(2);
        },
        addColumn: function () {
            let _this = this;
            if (typeof _this.tables[_this.currentTable] !== 'undefined') {
                let data = _this.tables[_this.currentTable].data;

                _this.tables[_this.currentTable].data.push({
                    name: '',
                    pk: false,
                    ai: false,
                    id: this.generateID()
                })
            }
        },
        removeColumn: function () {
            let _this = this;
            _this.tables[_this.currentTable].data.splice(_this.currentColumn, 1);
            if (_this.currentColumn > 0) {
                _this.currentColumn = parseInt(_this.currentColumn) - 1;
            } else {
                _this.currentColumn = 0;
            }
            if (_this.tables[_this.currentTable].data.length < 1) {
                _this.tables.splice(_this.currentTable, 1);
                _this.currentTable = 0;
            }
        },
        changePK: function (value) {
            if (value === true) {
                let _this = this;
                _this.tables[_this.currentTable].data.forEach(function (column, index) {
                    if (index !== _this.currentColumn) {
                        column.pk = false;
                    }
                })
            }
        },
        onDrag: function (x, y) {
            let _this = this;
            if (typeof _this.tables[_this.currentTable] !== 'undefined') {
                _this.tables[_this.currentTable].x = x;
                _this.tables[_this.currentTable].y = y;
                _this.drawRelation();
            }
        },
        onDragEvent: function(x, y) {
            let _this = this;
            let wait = false;
                if (!wait) {
                    _this.onDrag(x,y);
                    wait = true;
                    setTimeout(function () {
                        wait = false;
                    }, 1000);
                }
        },
        onActivate: function (index) {
            this.currentTable = index;
            this.addRelations();
        },
        addRelations: function () {
            let _this = this;
            let startRelation = _this.startRelation;
            let toRelation = _this.currentTable;
            if (startRelation !== toRelation && startRelation !== '' && typeof _this.tables[_this.startRelation] !== 'undefined') {
                if (_this.tables[startRelation].relations.findIndex(obj => obj.index == toRelation) < 0) {
                    _this.tables[startRelation].relations.push({
                        index: toRelation,
                        w: '',
                        h: '',
                        l: '',
                        t: '',
                        b: '',
                        r: '',
                        classes: '',
                    });
                }
                _this.drawRelation();
            }
        },
        drawRelation: function () {
            let _this = this;
            let tables = _this.tables;
            tables.forEach(function (table, index) {
                if (typeof table.relations !== 'undefined') {
                    table.relations.forEach(function (relation) {
                        let target = tables[relation.index];
                        let start = {
                            x: table.x,
                            y: table.y,
                            w: 120,
                            h: table.data.length * 28,
                        };
                        let end = {
                            x: target.x,
                            y: target.y,
                            w: 120,
                            h: target.data.length * 28,
                        };

                        let relationW = Math.abs(end.x - start.x) - 120;
                        if (relationW < 0) {
                            relationW = 1;
                        }
                        let tableH = table.data.length * 28 - 14;
                        let relationH = Math.abs(end.y - start.y);

                        relation.classes = '';

                        if (end.y > start.y) {
                            relation.t = '0px';
                            if (end.x > start.x) {
                                relation.classes = 'hide-top-right';
                            } else {
                                relation.classes = 'hide-top-left';
                            }
                        } else {
                            relation.b = '0px';
                            relation.t = 'auto';
                            relationH = relationH + tableH;
                            if (end.x > start.x) {
                                relation.classes = 'hide-bot-right';
                            } else {
                                relation.classes = 'hide-bot-left';
                            }
                        }
                        if (end.x > start.x) {
                            relation.l = '100%';
                        } else {
                            relation.l = 'auto';
                            relation.r = '100%';
                        }

                        relation.w = relationW + 'px';
                        relation.h = relationH + 'px';
                    });
                }
            });
        },
        initRelation: function (index) {
            this.startRelation = index;
        },
        documentClick: function (e) {
            let isTable = false;
            let target = e.target.className;

            this.selectedColumn = target.includes('q-td');
            if(!target.includes('relation')){
                this.selectedRelation = [];
            }
            else {
                let width = Math.abs(e.target.offsetWidth);
                let height = Math.abs(e.target.offsetHeight);
                let offsetX = e.offsetX;
                let offsetY = e.offsetY;
                console.log(width, height, offsetX, offsetY);
                if(offsetY > 10 && offsetX > 10){
                    this.selectedRelation = [];
                }
            }
            if(target.includes('q-table__top') || target.includes('q-table__title')) {
                this.selectedTable = true;
            }
            else {
                this.selectedTable = false;
            }
            e.path.forEach(function (el) {
                if (typeof el.className !== 'undefined' && (el.className === 'q-table' || el.className.includes('add-relation'))) {
                    isTable = true;
                    return false;
                }
            });
            if (!isTable) {
                this.startRelation = '';
            }
        },
        pressButton: function (e) {
            let _this = this;
            if(e.key === 'Delete' && typeof _this.tables[_this.currentTable] !== 'undefined'){
                if(_this.selectedColumn) {
                    _this.tables[_this.currentTable].data.splice(_this.currentColumn, 1);
                    _this.currentColumn = 0;
                    if (_this.tables[_this.currentTable].data.length < 1) {
                        _this.tables.splice(_this.currentTable, 1);
                        _this.currentTable = 0;
                    }
                }
                else if(_this.selectedTable) {
                    _this.tables.splice(_this.currentTable, 1);
                    _this.currentTable = 0;
                }
                else if(_this.selectedRelation.length > 1) {
                    let table = _this.selectedRelation[0];
                    let relation = _this.selectedRelation[1];
                    _this.tables[table].relations.splice(relation, 1);
                    _this.selectedRelation = [];
                }
            }
        },
        throttle: function (callback, limit) {
            var wait = false;
            return function (...args) {
                if (!wait) {
                    callback(...args);
                    wait = true;
                    setTimeout(function () {
                        wait = false;
                    }, limit);
                }
            }
        },
    },
    created() {
        document.addEventListener('click', this.documentClick);
        document.addEventListener('keydown', this.pressButton);
    },
    destroyed() {
        document.removeEventListener('click', this.documentClick);
        document.removeEventListener('keydown', this.pressButton);
    }
});