# DB MODEL UI/UX PROTOTYPE

## First phase

The goal of this repo is to test quasar as a user interface for database schema diagram editor.
- This is basic setup for you to get started fast. 
- This is only get have a POC for UX.
- Dont make this a proper vuejs app installable with NPM install. Only use index.html file. (The real app will be done later)
- This Is only to "FEEL" how Quasar works for a user.

The most important now is to get the diagram editor to work and feel good in the perspective of the user.
That is, it should "feel" good and fun to move around tables. It should "feel" great to connect a relationship. 

- (*Optional Suggestion) Maybe when placing tables, small subtle animation to the size of a box, it can shink/grow with 1% for 0.5 second so it feels for the user he "Drops" something?
- (*Optional Suggestion) Maybe the line between two tables will have a small animation when connected, like grow/shrink with 10% for 1.0 second so it feels the relationship is strong and a bond is created.

Deliverable:
-  Add tables
-  Add columns to tables
-  Change type of a column (dropdown) (int, string, enum)
-  mark/unmark properties of a column (checkboxes) (AI, PK, NN)
-  Change name/title of the column (input field, text)
-  Delete table (press delete on keyboard while table is marked)
-  Delete columns (press delete on keyboard while column is marked)
-  Add relationships between tables. (drag lines from either the left or right side of a table)
-  Delete relationship (press delete on keyboard while relationship is marked)

The Relationships:
The "Doctrine" library relationships should be used:
https://www.doctrine-project.org/projects/doctrine-orm/en/2.8/reference/association-mapping.html
But for this POC, only create basic relationship.

-  The "Has" relationships is drawn from the right of the table. This is child relationships.
-  The "Belongs" relationships is connected from the left to the table. This is "parent" relationships.

## Second phase

This phase is to make sure the relationships works as expeceted.

## Third phase 

This phase is to add additional attributes to the tables in other tabs.

## Fourth phase 
This phase is about forward engineer tables to sql

## Fifth phase
This is about importing existing tables into diagram editor


