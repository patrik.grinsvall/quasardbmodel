var workspace = new Vue({
	el: '#db-drawer',
	data: function(){
		return {
			currentTable: 0,
			currentColumn: 0,
			startRelation: {},
			tables: [],
			columns: [
				{
					name: 'name',
					field: 'name',
					align: 'left'
				},
			],
			columnTypes: ['int', 'string', 'enum'],
			columnAttributes: [
				{
					id: 'pk',
					label: 'PRIMARY KEY',
					single: true,
					default: false
				},
				{
					id: 'ai',
					label: 'AUTO INCREMENT',
					single: false,
					default: false
				},
				{
					id: 'nn',
					label: 'NOT NULL',
					single: false,
					default: false
				}
			],
			selectedTable: false,
			selectedColumn: false,
			selectedRelation: [],
			wait: false,
			startPoint: {},
			lineStyles: {},
			isDrag: {},
			dragContainerStyles: {},
			dragCancel: false,
		}
	},
	mounted() {
		this.dragContainerStyles = this.$refs.dragContainer.getBoundingClientRect();
	},
	methods: {
		showSettings: function(index, id){
			let _this = this;
			_this.currentColumn = _this.tables[_this.currentTable].columns.findIndex(obj => obj.id == id);
		},
		emptyColumn: function(name = '', type = 'string'){
			let attributes = {};
			this.columnAttributes.forEach((attribute) => {
				attributes[attribute.id] = attribute.default;
			});
			return {
				name: name,
				type: type,
				id: this.generateID(),
				attributes: attributes,
			}
		},
		addTable: function(){
			let _this = this;
			let newTable = {
				id: _this.generateID(),
				title: 'table ' + (_this.tables.length + 1),
				relations: [],
				x: 0,
				y: 0,
				active: false,
				columns: [
					_this.emptyColumn()
				]
			};
			_this.tables.push(newTable);
		},
		generateID: function(){
			return Math.random().toString(36).substr(2);
		},
		addColumn: function(table = '', name = ''){
			let _this = this;
			let tableIndex = table !== '' ? table : _this.currentTable;
			if(typeof _this.tables[tableIndex] !== 'undefined'){
				_this.tables[tableIndex].columns.push(_this.emptyColumn(name))
			}
		},
		removeColumn: function(){
			let _this = this;
			_this.tables[_this.currentTable].columns.splice(_this.currentColumn, 1);
			if(_this.currentColumn > 0){
				_this.currentColumn = parseInt(_this.currentColumn) - 1;
			}
			else {
				_this.currentColumn = 0;
			}
			if(_this.tables[_this.currentTable].columns.length < 1){
				_this.tables.splice(_this.currentTable, 1);
				_this.currentTable = 0;
			}
		},
		changeAttr: function(value, attributeId, single){
			if(value === true && single){
				let _this = this;
				_this.tables[_this.currentTable].columns.forEach(function(column, index){
					if(index !== _this.currentColumn){
						_this.$set(_this.tables[_this.currentTable].columns[index].attributes, attributeId, false);
					}
				})
			}
		},
		onDrag: function(x, y, drag = false){
			let _this = this;
			if(typeof _this.tables[_this.currentTable] !== 'undefined'){
				if(!drag){
					_this.checkIsOver(x, y);
				}
				_this.drawRelation();
			}
		},
		onDragEvent: function(x, y){
			let _this = this;
			if(!_this.wait){
				_this.onDrag(x, y, true);
				_this.wait = true;
				setTimeout(function(){
					_this.wait = false;
				}, 20);
			}
		},
		checkIsOver: function(x, y){
			let _this = this;
			if(typeof _this.tables[_this.currentTable] !== 'undefined'){
				let tables = [];
				let currentTable = {
					x: _this.tables[_this.currentTable].x,
					y: _this.tables[_this.currentTable].y,
					w: 120,
					h: (_this.tables[_this.currentTable].columns.length + 1) * 28,
				};

				_this.tables.forEach(function(table, index){
					if(index !== _this.currentTable){
						tables.push({
							x: table.x,
							y: table.y,
							w: 120,
							h: (table.columns.length + 1) * 28,
						});
					}
				});
				let isOver = false;
				let overElements = {};
				tables.forEach(function(table){
					let result = _this.isOver(currentTable, table);
					if(result.result){
						isOver = true;
						overElements = result;
						return false;
					}
				});
				if(isOver){
					let yDiff = Math.abs(overElements.el1.y - overElements.el2.b);
					let xDiff = Math.abs(overElements.el1.r - overElements.el2.x);
					let move = {
						direction: 'left',
						offset: 10
					};

					if(xDiff < yDiff){
						move.offset = xDiff + 10;
					}
					else {
						if((overElements.el1.r - overElements.el2.r) > yDiff) {
							move.direction = 'right';
							move.offset = overElements.el2.r - overElements.el1.x + 10;
						}
						else {
							move.direction = 'top';
							if(overElements.el1.y > overElements.el2.y) {
								move.offset = yDiff + 10;
							}
							else {
								move.offset = -overElements.el1.b + overElements.el2.y - 10;
							}
						}
					}
					if(move.direction === 'left'){
						_this.tables[_this.currentTable].x = currentTable.x - move.offset;
						setTimeout(function(){
							_this.checkIsOver(x - move.offset, y);
						}, 10);
					}
					else if(move.direction === 'top') {
						_this.tables[_this.currentTable].y = currentTable.y + move.offset;
						setTimeout(function(){
							_this.checkIsOver(x, y + move.offset);
						}, 10);
					}
					else {
						_this.tables[_this.currentTable].x = currentTable.x + move.offset;
						setTimeout(function(){
							_this.checkIsOver(x + move.offset, y);
						}, 10);
					}

				}
			}
		},
		isOver: function(el1, el2){
			el1.b = el1.y + el1.h;
			el1.r = el1.x + el1.w;
			el2.b = el2.y + el2.h;
			el2.r = el2.x + el2.w;

			let isOver = !((el1.b < el2.y) ||
				(el1.y > el2.b) ||
				(el1.r < el2.x) ||
				(el1.x > el2.r));
			return {
				result: isOver,
				el1: el1,
				el2: el2,
			};
		},
		onActivate: function(index){
			let _this = this;

			_this.currentTable = index;
			let x = _this.tables[index].x;
			let y = _this.tables[index].y;
			let startPosition = {
				x: x,
				y: y,
			};
			_this.$set(_this.tables[index], 'startPosition', startPosition);
			_this.addRelations();
		},
		addRelations: function(){
			let _this = this;
			let startRelation = _this.startRelation;
			let startRelationIndex = typeof startRelation.index !== 'undefined' ? startRelation.index : '';
			let toRelation = _this.currentTable;
			if(startRelationIndex !== toRelation && startRelationIndex !== '' && typeof _this.tables[startRelationIndex] !== 'undefined'){
				if(!startRelation.parent){
					toRelation = startRelationIndex;
					startRelationIndex = _this.currentTable;
				}
				_this.addColumn(toRelation, _this.tables[startRelationIndex].title + '_id');
				if(_this.tables[startRelationIndex].relations.findIndex(obj => obj.index == toRelation) < 0){
					_this.tables[startRelationIndex].relations.push({
						index: toRelation,
						w: '',
						h: '',
						l: '',
						t: '',
						b: '',
						r: '',
						classes: '',
					});
				}
				_this.startRelation = {};
				_this.drawRelation();
			}
		},
		moveLine: function(x1, y1, x2, y2){
			let _this = this;
			if(x2 < x1){
				let tmp;
				tmp = x2;
				x2 = x1;
				x1 = tmp;
				tmp = y2;
				y2 = y1;
				y1 = tmp;
			}

			let lineLength = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
			let m = (y2 - y1) / (x2 - x1);

			let degree = Math.atan(m) * 180 / Math.PI;
			let lineStyles = {
				'width': lineLength + 'px',
				'top': (y1) + 'px',
				'left': (x1) + 'px',
				'transform': 'rotate(' + degree + 'deg)',
				'display': 'block',
			};
			_this.lineStyles = lineStyles;
		},
		drawRelation: function(){
			let _this = this;
			let tables = _this.tables;
			tables.forEach(function(table, index){
				if(typeof table.relations !== 'undefined'){
					table.relations.forEach(function(relation){
						let target = tables[relation.index];
						let start = {
							x: table.x,
							y: table.y,
							w: 120,
							h: table.columns.length * 28,
						};
						let end = {
							x: target.x,
							y: target.y,
							w: 120,
							h: target.columns.length * 28,
						};

						let relationW = Math.abs(end.x - start.x) - 120;
						if(relationW < 0){
							relationW = 1;
						}
						let tableH = table.columns.length * 28 - 14;
						let relationH = Math.abs(end.y - start.y);

						relation.classes = '';

						if(end.y > start.y){
							relation.t = '0px';
							if(end.x > start.x){
								relation.classes = 'hide-top-right';
							}
							else {
								relation.classes = 'hide-top-left';
							}
						}
						else {
							relation.b = '0px';
							relation.t = 'auto';
							relationH = relationH + tableH;
							if(end.x > start.x){
								relation.classes = 'hide-bot-right';
							}
							else {
								relation.classes = 'hide-bot-left';
							}
						}
						if(end.x > start.x){
							relation.l = '100%';
						}
						else {
							relation.l = 'auto';
							relation.r = '100%';
						}

						relation.w = relationW + 'px';
						relation.h = relationH + 'px';
					});
				}
			});
		},
		initRelation: function(index, parent){
			this.startRelation = {
				'index': index,
				'parent': parent
			};
		},
		documentClick: function(e){
			let isTable = false;
			let target = e.target.className;

			this.selectedColumn = target.includes('q-td');
			if(!target.includes('relation')){
				this.selectedRelation = [];
			}
			else {
				let width = Math.abs(e.target.offsetWidth);
				let height = Math.abs(e.target.offsetHeight);
				let offsetX = e.offsetX;
				let offsetY = e.offsetY;
				let removeRelation = false;
				if(target.includes('hide-bot-right')){
					if(offsetY > 10 && offsetX > 10){
						removeRelation = true;
					}
				}
				else if(target.includes('hide-top-right')){
					if((height - offsetY) > 10 && offsetX > 10){
						removeRelation = true;
					}
				}
				else if(target.includes('hide-bot-left')){
					if(offsetY > 10 && (width - offsetX) > 10){
						removeRelation = true;
					}
				}
				else if(target.includes('hide-top-left')){
					if((height - offsetY) > 10 && (width - offsetX) > 10){
						removeRelation = true;
					}
				}
				if(removeRelation){
					this.selectedRelation = [];
				}

			}
			if(target.includes('q-table__top') || target.includes('q-table__title')){
				this.selectedTable = true;
			}
			else {
				this.selectedTable = false;
			}
			if(target.includes('add-relation')){
				let startPoint = {
					x: e.clientX,
					y: e.clientY,
				};
				this.startPoint = startPoint;
			}
			e.path.forEach(function(el){
				if(typeof el.className !== 'undefined' && (el.className === 'q-table' || el.className.includes('add-relation'))){
					isTable = true;
					return false;
				}
			});
			if(!isTable){
				this.startRelation = {};
			}
		},
		pressButton: function(e){
			let _this = this;
			if(e.key === 'Delete' && typeof _this.tables[_this.currentTable] !== 'undefined'){
				if(_this.selectedColumn){
					_this.tables[_this.currentTable].columns.splice(_this.currentColumn, 1);
					_this.currentColumn = 0;
					if(_this.tables[_this.currentTable].columns.length < 1){
						_this.tables.splice(_this.currentTable, 1);
						_this.currentTable = 0;
					}
				}
				else if(_this.selectedTable){
					_this.tables.splice(_this.currentTable, 1);
					_this.currentTable = 0;
				}
				else if(_this.selectedRelation.length > 1){
					let table = _this.selectedRelation[0];
					let relation = _this.selectedRelation[1];
					_this.tables[table].relations.splice(relation, 1);
					_this.selectedRelation = [];
				}
			}
		},
		cancelDrag: function(event){
			let _this = this;
			if(typeof _this.tables[_this.currentTable] !== 'undefined'){
				_this.dragCancel = true;
				let table = _this.tables[_this.currentTable];
				let x = 0;
				let y = 0;
				if(typeof table.startPosition !== "undefined"){
					x = table.startPosition.x;
					y = table.startPosition.y;
				}
				setTimeout(function(){
					_this.$set(_this.tables[_this.currentTable], 'x', x);
					_this.$set(_this.tables[_this.currentTable], 'y', y);
				}, 100);
			}
			this.isDrag = {};
		},
		initDrag: function(e, index){
			if(!e.target.className.includes('add-relation') && !e.target.className.includes('relation')){
				this.dragCancel = false;
				this.isDrag = {
					index: index,
					offsetX: e.offsetX,
					offsetY: e.offsetY,
				};
				this.onActivate(index);
			}
		},
		drag: function(e){
			let _this = this;
			let drag = _this.isDrag;

			if(typeof drag.index !== 'undefined' && typeof _this.tables[drag.index] !== 'undefined'){
				let parentStyles = _this.dragContainerStyles;
				let x = e.pageX - parentStyles.left - drag.offsetX;
				let y = e.pageY - parentStyles.top - drag.offsetY;

				_this.tables[drag.index].x = x;
				_this.tables[drag.index].y = y;
				_this.onDragEvent(x, y)
			}
			if(typeof _this.startRelation.index !== 'undefined'){
				let active = false;
				e.path.forEach((el) => {
					if(el.className === 'tables'){
						active = true;
					}
				});
				if(active && typeof _this.startPoint.x !== 'undefined' && typeof _this.startPoint.y !== 'undefined'){
					_this.moveLine(_this.startPoint.x, _this.startPoint.y, e.clientX, e.clientY);
				}
			}
		},
		dragStop: function(e){
			let drag = this.isDrag;
			console.log('drags');
			let parentStyles = this.dragContainerStyles;
			if(typeof drag.offsetX !== 'undefined'){
				let x = e.pageX - parentStyles.left - drag.offsetX;
				let y = e.pageY - parentStyles.top - drag.offsetY;
				this.onDrag(x, y);
				this.isDrag = {};
			}
		},
	},
	created(){
		document.addEventListener('click', this.documentClick);
		document.addEventListener('keydown', this.pressButton);
	},
	destroyed(){
		document.removeEventListener('click', this.documentClick);
		document.removeEventListener('keydown', this.pressButton);
	}
});